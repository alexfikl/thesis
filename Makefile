TEXMK?=latexmk
OUTDIR?=latex.out
TEXFLAGS:=-pdf -shell-escape -output-directory=$(OUTDIR)

TEXS:=$(wildcard ch*.tex)
BIB=thesis.bib

all: thesis.pdf

%.pdf: %.tex $(TEXS) $(BIB) thesis-style.sty uiucthesis2014.cls
	@mkdir -p $(OUTDIR)
	$(TEXMK) $(TEXFLAGS) $<
	@cp $(OUTDIR)/$@ .

clean:
	@rm -rf *.log *.out *.bbl *.spl \
		*.sync* *.blg *.aux *.fls *.fdb_latexmk \
		$(OUTDIR)

purge: clean
	@rm -rf *.pdf

.PHONY: clean purge
