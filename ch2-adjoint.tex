\chapter{Adjoint-based Optimization}
\label{ch:adjoint}

In this chapter we will attempt to give a very broad view on the optimization
and optimal control of flows with PDE-based constraints with a focus on
adjoint-based methods. The problems discussed here take a very general form:
\begin{equation} \label{eq:ch2:minimization}
\left\{
\begin{aligned}
& \min_{u, g} \mathcal{J}(u, g), \\
& \mathcal{F}(u, g) = 0
\end{aligned}
\right.
\end{equation}
where we can identify the main components of a constrained optimization problem:
\begin{itemize}
    \item $u$, known as the \emph{state variables}. In the setting of fluid
    mechanics, state variables usually represent the velocity field (alternatively
    represented by a velocity potential or stream function), pressure, density,
    temperature, etc.
    \item $g$, known as the \emph{control variables} or \emph{design parameters}
    that are commonly part of a constraint set $G_{\mathrm{ad}}$. There are also
    many examples of control variables, of which maybe the most well known
    ones are parameters determining the shape of an object in design optimization.
    Other examples include the heat flux or inflow mass rate through a wall, etc.
    \item $\mathcal{F}(u, g)$ is the set of \emph{(PDE-based) constraints} that
    both the state variable $u$ and the control variable $g$ must satisfy. For
    the type of flow optimization problems we are interested in, the constraints
    will usually be the Euler or Navier-Stokes equations or simpler models like
    the linear advection equation or Burgers' equation.
    \item $\mathcal{J}$ is the \emph{cost} or \emph{objective functional}.
    Common types of cost functionals include tracking-type functionals, which
    match the solution to a desired result at a certain time (or for all time),
    lift maximization or drag minimization functionals, etc.
\end{itemize}

Issues regarding the existence and uniqueness of a solution $\hat{g}$ to such
problems are very complex and will not be discussed here. For an in-depth
analysis we direct the reader to classic materials such as~\cite{L1971} or
more recent works such as~\cite{G2003},~\cite{T2010} or ~\cite{BS2012}.

In the rest of this chapter we will focus on defining a set of approaches
to solve the above optimization problem. The problem will be kept abstract
as much as possible with explicit examples given in the chapters to come.
Furthermore, for now we will assume that both the cost functional $\mathcal{J}$
and the constraints $\mathcal{F}$ are differentiable (in a certain sense), as
issues regarding non-differentiability will be discussed later.

\section{Calculus of Variations}

To avoid any ambiguity, we will first give an incomplete introduction to the
ideas of the calculus of variations, with the main aim of introducing
the notation and definitions we will be using for the rest of the chapter.

The most important component of the calculus of variations is the idea of
a directional derivative, of which there are numerous types throughout the
literature. We will focus here on two of them: the Gâteaux derivative and the
Fréchet derivative. Firstly, consider a mapping $f$ from $X$ to $Y$, two
affine vector spaces:
\[
f : X \longrightarrow Y
\]
If it exists, the Gâteaux derivative of $f$ at a point $x$ in the direction
$y$ is given by the limit:
\[
\od{f}{x}(x; y) = \lim_{\epsilon \to 0}
\frac{f(x + \epsilon y) - f(x)}{\epsilon},
\]
in which case we call $f$ Gâteaux-differentiable. Note that the Gâteaux
derivative defined in this way is not necessarily linear in $y$, as we would
expect from a derivative operator. However, it must still be homogeneous of
the first degree, in the the sense that $\mathrm{d}f(x; \alpha y) =
\alpha \mathrm{d}f(x; y)$.

Many of the applications we will be looking at will require a slightly stronger
notion of differentiability than that given by Gâteaux derivatives, namely the
implication that if the directional derivative exists at a given point, the
mapping must be continuous at that point. To allow for this, we must impose
further structure on our vector spaces $X$ and $Y$: they must be complete
normed vector spaces, i.e. Banach spaces. Consider now the mapping
$f: X \to Y$ defined on $X, Y$ Banach spaces endowed with the norms $\|\cdot\|_X$
and $\|\cdot\|_Y$, respectively. If, for a fixed $x \in X$, there exists a
bounded linear operator $\mathrm{d}f(x)$ such that:
\[
\lim_{y \to 0}
\frac{\|f(x + y) - f(x) - \mathrm{d}f(x; y)\|_Y}{\|y\|_X} = 0,
\]
then $\mathrm{d}f(x; y)$ is called the Fréchet-derivative of $f$ at $x$ in the
direction $y$. As mentioned, this is a stronger condition on $f$ and, as such,
a Fréchet-differentiable mapping is necessarily Gâteaux-differentiable, in the
sense that we have defined them. From now on we will only be using the term
directional derivative to refer to the Fréchet derivative of a mapping. To
reiterate, we will be using the following notation to denote a Fréchet
derivative of a mapping with one variable at a point $x$ in the direction $y$:
\[
\od{f}{x}(x; y)
\quad \text{or} \quad
\mathrm{d}f(x; y),
\]
often forsaking the mention of the direction to refer to the derivative operator
itself. If the mapping takes multiple variables, for example in a product space
$X_1 \times X_2$, we denote its Fréchet derivatives with the following common
notation for partial derivatives:
\[
\pd{f}{x_1}(x_1, x_2; y_1, 0)
\quad \text{and} \quad
\pd{f}{x_1}(x_1, x_2; 0, y_2)
\]
at a point $(x_1, x_2)$ in the direction $(y_1, 0)$ and $(0, y_2)$, respectively.
Note that these are not what we understand by partial derivatives in the classical
sense, but simply denote the usual Frechét derivative in a particular set of
directions.

A very important special case is that of functionals, i.e. mappings
to the real line with $Y = \mathbb{R}$. This case allows us to return to the
familiar ground of derivatives of multivariate mappings by allowing the
definition of a \emph{gradient}. To define the gradient we will also assume that
$X$ is a Hilbert space (inner product spaces could also used, but they are not
sufficient for uniqueness) and use the \emph{Riesz representation theorem}
which gives us:
\[
\od{f}{x}(x; y) = (\nabla f(x), y),
\]
where $(\cdot, \cdot)$ is the inner product on $X$ and $\nabla f(x)$ is
the gradient of $f$ at $x$. The spaces we are usually interested in, such as
$\mathrm{L}^2$ or $\mathrm{H}^k$ (i.e. the Sobolev spaces $\mathrm{W}^{k, 2}$),
are all Hilbert spaces, so we will usually be able to define the gradient in
such a way. Note that, in the case of Hilbert spaces, the Riesz Representation
theorem basically gives the equivalence $X^* = X$, but this is not to be
understood as set equality. The equality is in the sense that there exists a
norm-preserving isomorphism between the two spaces, which is what the theorem
tells us.

Note, however, that computing the directional derivative of $f$ is
usually quite straightforward, but finding the gradient can prove to be more
challenging depending on the choice of Hilbert space. Another important
consequence of defining the gradient in such a way is that the definition
invariably depends on the choice of inner product. This can have a very large
impact on the convergence of an optimization algorithm and the resulting
solution. For example, using the standard inner product on $\mathrm{H}^1$ will
introduce derivatives into the inner product which can have a smoothing effect
on the result. For a deeper discussion and examples of this phenomenon,
see~\cite{BS2012}.

In the case of functionals on a Hilbert space, we can also make use of dual
space $X^*$ of $X$ and note that the directional derivative is a mapping into
the dual space:
\[
\od{f}{x}: X \longrightarrow X^*,
\]
which, when applied to a given point, becomes an element of that dual space:
\[
\od{f}{x}(x; \cdot): X \longrightarrow \mathbb{R}
\]
and, similarly, the gradient is an endomorphism on $X$:
\[
\nabla f: X \longrightarrow X,
\]
such that the gradient at a point is an element of the space itself $\nabla f(x)
\in X$. These are very important considerations, since the differences between
directional derivatives and gradients are often a matter of confusion.

Getting back to our optimization problem~\eqref{eq:ch2:minimization}, we will
assume that the state variables $u \in U$ are elements of a given Hilbert
space and, similarly, the constraints $g \in G_{\mathrm{ad}} \subset G$ are at
least in a convex subset of a given Hilbert space $G$ ($G_{\mathrm{ad}}$ can
also be a subspace or the full space, when there are no constraints on $g$). These
assumptions will apply to most, if not all, of the practical examples we are
interested in. Given these definitions our cost functional is defined as the
mapping:
\begin{equation}
\mathcal{J}: U \times G \longrightarrow \mathbb{R},
\end{equation}
and the constraints are given by a mapping:
\begin{equation} \label{eq:ch2:constraints_fn}
\mathcal{F}: U \times G \longrightarrow P,
\end{equation}
where $P$ is a Hilbert space as well, whose use will become important in
defining the adjoint equations. Since $\mathcal{J}$ is a functional on a
Hilbert space (product space of two Hilbert spaces), all the definitions of
the directional derivatives from above will apply. More importantly, we can
also apply the \emph{chain} rule to obtain:
\begin{equation} \label{eq:ch2:cost_grad_chain}
\od{\mathcal{J}}{g}(u(g), g) =
\pd{\mathcal{J}}{u}(u, g) \od{u}{g}(g) +
\pd{\mathcal{J}}{g}(u, g).
\end{equation}
We note that we can consider $\mathcal{J}$ as a single variable functional by
using the implicit function theorem to motivate the existence of $u(g)$ such
that $\mathcal{F}(u, g) = 0$. In the chain rule above, we have mentioned the
following directional derivatives which belong to their respective dual spaces:
\[
\frac{\partial \mathcal{J}}{\partial g}(u, g) \in G^*
\quad \text{and} \quad
\frac{\partial \mathcal{J}}{\partial u}(u, g) \in U^*,
\]
as well as
\[
\od{u}{g}(g): G \longrightarrow U,
\]
which is a bounded linear operator, but it does not belong to a dual space.
However, the composition of the two operators does:
\[
\pd{\mathcal{J}}{u} \od{u}{g} \in G^*,
\]
as it must, since the left-hand side is part of the dual space of $G$. We
also introduce the following gradient notation corresponding to the various
directional derivatives:
\begin{itemize}
    \item $\nabla_u \mathcal{J}(u, g)$ is the gradient with respect to $u$,
    while keeping $g$ constant and
    \item $\nabla_g \mathcal{J}(u, g)$ is the gradient with respect to $g$,
    while keeping $u$ constant,
\end{itemize}
with the gradients being obtained using the Riesz representation theorem, as
defined in the more general setting.

\section{Differentiate-then-discretize}

Having defined all the necessary terms, we can proceed to derive a generic
formulation of the so-called optimality system and the adjoint method. The
first step in this derivation is making use of \emph{Lagrange relaxation} (also
known as the Lagrange multiplier method) to transform the constrained
optimization problem~\eqref{eq:ch2:minimization} into an unconstrained
optimization problem. This can be accomplished by introducing the Lagrangian
(or Lagrange functional):
\begin{equation} \label{eq:ch2:lagrangian}
\mathcal{L}(u, g, p) = \mathcal{J}(u, g) - (p, \mathcal{F}(u, g))_P,
\end{equation}
where $(\cdot, \cdot)_P$ is the inner product of $P$, introduced as the image
of~\eqref{eq:ch2:constraints_fn}, and $p \in P$ is called a \emph{Lagrange
multiplier}, which is used to weakly impose the constraints as part of the
functional. Note that the existence of the Lagrange multiplier $p$ is not
guaranteed and is usually dependent on the surjectivity of $\mathcal{F}$, which
we will assume from now on. Now that we have an unconstrained problem, we can
define the usual first-order necessary conditions for a minimum:
\[
\begin{cases}
\nabla_u \mathcal{L}(u, g, p) = 0, \\
\nabla_g \mathcal{L}(u, g, p) = 0, \\
\nabla_p \mathcal{L}(u, g, p) = 0. \\
\end{cases}
\]

We have separated the derivatives of the Lagrangian with respect to each of
the variables of interest because each of them will give an important part
of the optimality system, as we will see. We will start with the last condition,
since it is the simplest one. We have the following directional derivative:
\[
(\nabla_p \mathcal{L}, \tilde{p})_P =
\pd{\mathcal{L}}{p}(u, g, p; \tilde{p}) =
-(\tilde{p}, \mathcal{F}(u, g))_P =
-(\mathcal{F}(u, g), \tilde{p})_P,
\]
for an arbitrary admissible direction $\tilde{p}$, so our first-order
optimality condition reads:
\[
\nabla_p \mathcal{L} = -\mathcal{F}(u, g) = 0,
\]
i.e. the derivative with respect to the Lagrange multiplier $p$ simply retrieves
the original constraints. Next we will look at the derivative with respect
to the state variable $u$. We have that:
\[
(\nabla_u \mathcal{L}, \tilde{u})_U =
\pd{\mathcal{L}}{u}(u, g, p; \tilde{u}) =
\pd{\mathcal{J}}{u}(u, g; \tilde{u}) -
\left(p, \pd{\mathcal{F}}{u}(u, g; \tilde{u})\right)_P,
\]
where we can use the Riesz representation theorem for the first directional
derivative:
\[
\pd{\mathcal{J}}{u}(u, g; \tilde{u}) = (\nabla_u \mathcal{J}, \tilde{u})_U.
\]

However, transforming the inner product into a similar form is slightly more
complicated, since we have that:
\[
\pd{\mathcal{F}}{u}(u, g): U \longrightarrow P
\]
is a bounded linear operator and not a functional. In this scenario, we make
use of the definition of an adjoint operator:
\[
\left(\pd{\mathcal{F}}{u}(u, g)\right)^*: P \longrightarrow U
\]
which must satisfy:
\[
\left(p, \pd{\mathcal{F}}{u}(u, g; \tilde{u})\right)_P =
\left(\left(\pd{\mathcal{F}}{u}(u, g)\right)^*p, \tilde{u}\right)_U.
\]

The existence and uniqueness of the adjoint operator in a Hilbert space is a
consequence of the Riesz Representation theorem as well. Putting the two terms
back together, we obtain the gradient of the Lagrangian:
\[
\nabla_u \mathcal{L} =
\nabla_u \mathcal{J} - \left(\pd{\mathcal{F}}{u}\right)^*p = 0.
\]

Finally, we will look at the derivatives with respect to the control variable
$g$. Very similarly, we have that:
\[
(\nabla_g \mathcal{L}, \tilde{g})_G =
\pd{\mathcal{L}}{g}(u, g, p; \tilde{g}) =
\pd{\mathcal{J}}{g}(u, g; \tilde{g}) -
\left(p, \pd{\mathcal{F}}{g}(u, g; \tilde{g})\right)_P.
\]
Using the same reasoning as before, we obtain:
\[
\nabla_g \mathcal{L} =
\nabla_g \mathcal{J} - \left(\pd{\mathcal{F}}{g}\right)^*p = 0.
\]

The optimality system of the given optimization problem~\eqref{eq:ch2:minimization}
is given by the explicit form of the three first-order conditions that we
have derived this far. It is formed out of:
\begin{itemize}
    \item The constraints, or \textbf{state equations}:
\begin{equation} \label{eq:ch2:state}
    \mathcal{F}(u, g) = 0.
\end{equation}
    \item The \textbf{adjoint equations}:
\begin{equation} \label{eq:ch2:adjoint}
\left(\pd{\mathcal{F}}{u}\right)^*p = \nabla_u \mathcal{J}
\end{equation}
    \item The \textbf{optimality condition}:
\begin{equation} \label{eq:ch2:optimality}
\left(\pd{\mathcal{F}}{g}\right)^*p = \nabla_g \mathcal{J}
\end{equation}
\end{itemize}

There are at least two ways to solve the above system (see~\cite{G2003}). The
naive method is using what is known as a \emph{one-shot method}. In the case
of the system~\eqref{eq:ch2:state}-\eqref{eq:ch2:optimality}, we have $3$
unknowns $u, g, p$ and as many equations, so the existence of a solution is
indeed possible. However, our constraints can be complex nonlinear PDEs which
are very hard to solve, so we would expect the coupled system to be even
more formidable. This makes one-shot methods intractable in the case of PDE-based
constraints.

An alternative to using one-shot methods is using an iterative method to solve
the system. Such a method would require the gradient of the cost functional
as given in~\eqref{eq:ch2:cost_grad_chain}. However, this poses its own set of
problems since the term
\[
\od{u}{g}
\]
will have to be computed for every control variable $g$. In many applications,
such as shape optimization, we have a very large amount of control variables,
so finding the above derivative would require solving the constraint PDE system
for each variation. This is a valid approach, known as computing the gradient
through \emph{sensitivities} (of $u$ with respect to $g$). An alternative
method is the \emph{adjoint method}, which sidesteps the requirement of solving
a system of PDEs for each $g$ by instead solving the adjoint system given
by~\eqref{eq:ch2:adjoint}.

To obtain the adjoint formulation of the gradient, we will first look at the
derivatives of the constraints from~\eqref{eq:ch2:minimization} using the chain
rule as in~\eqref{eq:ch2:cost_grad_chain}:
\[
\begin{aligned}
&
\od{\mathcal{F}}{g}(u(g), g) =
\pd{\mathcal{F}}{u} \od{u}{g}(u, g) +
\pd{\mathcal{F}}{g}(u, g) = 0 \\
\implies &
\od{u}{g} = -\left(\pd{\mathcal{F}}{u}\right)^{-1} \pd{\mathcal{F}}{g},
\end{aligned}
\]
which we can actually replace into~\eqref{eq:ch2:cost_grad_chain} to obtain:
\[
\od{\mathcal{J}}{g}(u(g), g) =
\pd{\mathcal{J}}{g}(u, g) -
\pd{\mathcal{J}}{u}
\left(\pd{\mathcal{F}}{u}\right)^{-1} \pd{\mathcal{F}}{g} (u, g)
\]

We have a choice in the above formulation about how to associate the products
in the second term. Associating right to left:
\[
\pd{\mathcal{J}}{u}
\left\{\left(\pd{\mathcal{F}}{u}\right)^{-1} \pd{\mathcal{F}}{g}\right\},
\]
gives the sensitivity method, while associating left to right:
\[
\left\{\pd{\mathcal{J}}{u} \left(\pd{\mathcal{F}}{u}\right)^{-1} \right\}
\pd{\mathcal{F}}{g},
\]
gives the adjoint method. For the adjoint method, we define $p^* \in P^*$ such
that:
\[
p^* =
\pd{\mathcal{J}}{u} \left(\pd{\mathcal{F}}{u}\right)^{-1}: P \longrightarrow \mathbb{R}
\]
which gives:
\[
\od{\mathcal{J}}{g}(u(g), g) =
\pd{\mathcal{J}}{g}(u, g) -
p^* \pd{\mathcal{F}}{g} (u, g).
\]

We will now apply the Riesz representation theorem on this formulation where
we have denoted the Lagrange multiplier $p \in P$ as the representer of $p^*$:
\[
\begin{aligned}
(\nabla \mathcal{J}, \tilde{g})
& =
(\nabla_g \mathcal{J}, \tilde{g}) -
\left(p, \pd{\mathcal{F}}{g}(u, g; \tilde{g}) \right) \\
& =
(\nabla_g \mathcal{J}, \tilde{g}) -
\left(\left(\pd{\mathcal{F}}{g}\right)^* p, \tilde{g} \right)
\end{aligned}
\]
or:
\begin{equation} \label{eq:ch2:gradient}
\nabla \mathcal{J} = \nabla_g \mathcal{J} -
\left(\pd{\mathcal{F}}{g}\right)^* p.
\end{equation}
where $p$ still satisfies~\eqref{eq:ch2:adjoint}. We can see that we have now
eliminated the directional derivative of $u(g)$ from the system. Not only this,
but, to compute the gradient as given in~\eqref{eq:ch2:gradient}, we only need
to solve the adjoint system once to obtain $p$. The gradient we have obtained
here is exactly the same as the derivative of the Lagrangian $\mathcal{L}$ with
respect to $g$ that gives the optimality condition from~\eqref{eq:ch2:optimality}.
This is to be expected since incorporating the constraints into the Lagrangian
and incorporating them directly into the derivative of the cost functional is
completely equivalent.

See~\cite{BS2012} for further details about these results as well as how
to obtain second order conditions using the Hessian and~\cite{G2003} for
details on the one-shot method and obtaining the gradient using
sensitivities (as well as plenty of examples).

Since it is unlikely we will find an analytical solution to our optimization
problem, the next step will be discretizing the optimality system and the
gradient from~\eqref{eq:ch2:gradient} and using any of the well-known gradient
descent methods to solve the optimization problem.

The method we have presented here is known as the \emph{differentiate-then-discretize}
approach to adjoint-based optimization, referring to the order of the
operations: we have first derived the continuous optimality system which we
will then discretize accordingly. This method has its advantages:
\begin{itemize}
    \item We can use different numerical schemes to solve the state and adjoint
    equations;
    \item We can use different meshes;
    \item We can avoid differentiating numerical artifacts such as
    nonlinearities in the numerical scheme (e.g. limiters in MUSCL schemes),
    moving meshes, turbulence models, shock-capturing schemes, etc.
\end{itemize}

However, there is at least one significant downside to this approach. Since
we discretize the state and adjoint equations separately, there is no
guarantee that the discrete gradient will actually correspond to the continuous
one that we expect. Examples are given in~\cite{G2003} in which the
approximate gradient doesn't only not point in the direction of steepest
descent, it actually points completely in the opposite direction. This can
be remedied with regularization or other methods, but it is generally a very
important issue.

\section{Discretize-then-differentiate}

As a counterpart to the \emph{differentiate-then-discretize} approach we have
the \emph{discretize-then-differentiate} approach, which does the two steps
in reverse order (of course the differentiation and discretization operators
do not commute). In this case it is perhaps more intuitive to look at the
problem from a linear algebra point of view (see also~\cite{GP2000}) where
$\vect{u} \in \mathbb{R}^{N}$ is the discretized $u$ on a space-time grid
with a total of $N = N_t \times N_s$ discrete ``points''. Similarly $\vect{g}
\in \mathbb{R}^M$ is the set of discrete constraints. Unlike the state variables,
the constraints could already be a discrete set or we could have infinite
dimensional constraints which require discretization. The discrete PDE-based
constraints are then denoted by $\mathcal{F}^h(\vect{u}, \vect{g})$ and
the discrete cost functional is denoted by $\mathcal{J}^h(\vect{u}, \vect{g})$.
In this case, we can talk about derivatives in a much more familiar setting,
i.e. in $\mathbb{R}^n$ for some $n$, but the same rules apply as we have seen
in the first section of this chapter.

We still have the derivative of the cost functional obtained with the chain
rule as in~\eqref{eq:ch2:cost_grad_chain}:
\[
\od{\mathcal{J}^h}{\vect{g}} =
\pd{\mathcal{J}^h}{\vect{u}} \od{\vect{u}}{\vect{g}} +
\pd{\mathcal{J}^h}{\vect{g}}
\]
and the derivative of the discretized constraints:
\[
\od{\mathcal{F}^h}{\vect{g}} =
\pd{\mathcal{F}^h}{\vect{u}} \od{\vect{u}}{\vect{g}} +
\pd{\mathcal{F}^h}{\vect{g}} = 0.
\]
Discretely, we also have a choice of space that we can choose from (generally
this will be the Euclidean space) and an associate inner product (generally
given by a weighted inner product $(\vect{x}, \vect{y}) = \vect{x}^T \vect{My}$,
where $\vect{M}$ is a positive-definite matrix). This choice will also
influence the resulting formulation and gradient. To use the adjoint
method, we will once again reduce the two above equations to a single one:
\[
\od{\mathcal{J}^h}{\vect{g}} =
\pd{\mathcal{J}^h}{\vect{g}} +
\vect{h}^T \vect{v},
\]
where $v$ solves $\vect{Lv} = \vect{f}$ with the following notation:
\[
\begin{aligned}
\vect{v} =~ & \od{\vect{u}}{\vect{g}} \in \mathcal{M}_{N \times M},
& \quad \quad
\vect{h}^T =~ & \pd{\mathcal{J}^h}{\vect{u}} \in \mathbb{R}^N, \\
\vect{L} =~ & \pd{\mathcal{F}^h}{\vect{u}} \in \mathcal{M}_{N \times N},
& \quad \quad
\vect{f} =~& -\pd{\mathcal{F}^h}{\vect{g}} \in \mathcal{M}_{N \times M}.
\end{aligned}
\]

As before, we note that to solve the system $\vect{Lv} = \vect{f}$, we actually
have to solve $M$ different systems with $\vect{L}$ as the operator. This
corresponds to computing the gradient using sensitivities in the continuous
case. We can now introduce the adjoint variable $\vect{p} \in \mathbb{R}^N$
which solves $\vect{Lp} = \vect{h}$ so that our gradient becomes:
\[
\od{\mathcal{J}^h}{\vect{g}} =
\pd{\mathcal{J}^h}{\vect{g}} +
\vect{p}^T \vect{f}.
\]

The main advantage of this approach is that the gradient above is guaranteed
to be the gradient of the discrete cost functional, since the approximations
have been made beforehand. Because of the consistency of the gradient, this can
also lead to considerably improved convergence of any descent algorithm.
However, all the advantages of the \emph{differentiate-then-discretize} method
have disappeared here. Using different numerical schemes or meshes for the
state and adjoint equations is, of course, no longer possible and we are left
with linearizing complex numerical schemes, which can be a daunting task on
its own.

Because of these difficulties, a very important tool for adjoint-based optimization
is \emph{automatic differentiation}. It can automatically create the linearized
and adjoint code from the state equations and can save large amounts of time.
However, it does have its own downsides: as any automatic process, it is unlikely
to be as optimized as a manual implementation and will use both more storage
and more processing power in most non-trivial cases. A thorough comparison
between the two approaches can be found in~\cite{NJ2000}.
