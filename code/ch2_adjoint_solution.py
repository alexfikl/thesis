#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc

plt.style.use("seaborn-whitegrid")
rc('font', size=14)
rc('text', usetex=True)
rc('savefig', dpi=300)


def state(x, t):
    if x < (0.5 * t):
        return 1.0
    return 0.0


def adjoint(x, t):
    assert t <= 1.0
    if x < (-0.5 + t):
        return 1.0
    elif (-0.5 + t) <= x <= (0.5 + t):
        return 0.5
    return 0.0


if __name__ == "__main__":
    N = 1000
    T = 1.0

    x = np.linspace(-1.0, 1.0, N)
    fs = np.vectorize(state)
    fa = np.vectorize(adjoint)

    plt.axis([-1.0, 1.0, -0.01, 1.01])
    plt.title("$u$")
    plt.plot(x, fs(x, 1.0), linewidth=2)
    plt.savefig("ch2_solution_state.png", dpi=300)
    plt.clf()

    plt.axis([-1.0, 1.0, -0.01, 1.01])
    plt.title("$p$")
    plt.plot(x, fa(x, 0.0), linewidth=2)
    plt.savefig("ch2_solution_adjoint.png", dpi=300)
    plt.clf()

