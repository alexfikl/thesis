\chapter{Conclusions}
\label{ch:conclusions}

The work presented in this thesis deals with the most obvious issues that
can arise in the optimization of multiphase flows using the
discretize-then-differentiate approach to adjoint methods. In
general, we believe that a well-posed problem involving sharp interfaces
(at least in the limit) can be formulated in this scenario.

We have studied the ability of discrete adjoint schemes to correctly converge
to weak solutions with various types of discontinuities. The first case we
have looked at is related to contact discontinuities in the simplified setting
of linear advection equations. Discretely, even in this simple case, convergence
of the adjoint is not a given because we are differentiating numerical schemes
that can have any number of issues (e.g. highly nonlinear, non-differentiable,
etc.). We have seen that, for two commonly used numerical schemes, the adjoint
correctly advects the discontinuities backwards in time. However, issues with
the linearized fluxes keep the results from being as accurate as what we
would obtain by separately discretizing the adjoint equations, in the
differentiate-then-discretize fashion. Specifically, in the case of a flux
limited scheme, the adjoint is no longer monotonicity preserving and exhibits
the oscillations common to dispersive second-order schemes.

The second case we have looked at involves the appearance of shocks in
nonlinear hyperbolic conservation laws. The case of shocks is significantly more
complicated than that of contact discontinuities because first-order variations
of solutions to nonlinear hyperbolic conservation laws require us to take
the shock position into account. The combined first-order variation of the
actual solution and the shock position is called a generalized tangent vector.
Using the theory of generalized tangent vectors, we have presented the
derivation of the adjoint equations, which contained both equations for the
adjoint variable and the evolution of an adjoint shock position. The
main takeaway from this derivation is that the adjoint is necessarily continuous
along all characteristics leading into the shock. Furthermore, the adjoint
has an extra boundary condition at the shock position defining the value
along the aforementioned characteristics. Numerically, this can be approached
in two ways: the extra boundary condition can be imposed explicitly or special
numerical schemes can be designed to approximate the boundary condition
implicitly. We have studied the second case using a set of commonly used
schemes. We have seen that convergence to the correct weak adjoint solution
is possible if the shock is smeared over an increasing number of cells during
the forward solve. Unfortunately, most commonly used schemes do not have
this property, so, unsurprisingly, neither the classic upwind scheme nor a
flux limited scheme exhibit mesh convergence. However, the approximated
solutions oscillate around the correct exact solution, so in some practical
situations (e.g. weak shocks), these schemes can still be acceptable.

Another issue that was studied for both contact discontinuities and shocks
is related to the complete vs. incomplete differentiation of the numerical
fluxes. Since most numerical fluxes have non-differentiable parts, it may be
beneficial to simply consider some parts constant during differentiate,
especially if done manually. We have seen that, in the case of the fairly
nonlinear flux limited schemes, differentiating the flux limiter itself with
respect to the problem variables yielded a very small benefit. In the case
of the advection equation, complete differentiation even unearthed the
oscillations coming from the Lax-Wendroff flux used in the combination.

Having determined some general intuition about the effect of discontinuities
on the discrete adjoint equations, we have moved to another family of numerical
schemes more commonly used in multiphase flows. The THINC family of schemes
was chosen for our investigation because of its simplicity compared to
other commonly used anti-diffusive schemes and because all important components
of the scheme are differentiable. We have seen that the classic definition of
the THINC scheme leads to unexpected features in the adjoint, which are due
to the fact that the scheme is not well-defined in pure states. The main
issue was the need to switch to a different numerical scheme in pure states,
which leads to large discontinuities in the flux derivatives. We have solved
this problem by proving that the THINC scheme can be approximated, to leading
order, in the limit of pure states. An natural extension to the results regarding
the THINC scheme is to attempt to find a mollifier with well-defined values
in the limit of pure states, without the need for further modifications. To
this end, we present a new family of numerical schemes called COMIC, based on
compact mollifiers, in the sense that pure states are achieved in a finite
interval. The sine-based COMIC scheme we have presented is significantly
more complex than the THINC scheme, due to the use of root finding algorithms,
but it gives better results in some of the numerical tests we have performed,
notably for sharp interfaces.

The main result of our analysis of the anti-diffusive schemes is that the
linearized and adjoint equations converge everywhere except at the interface.
In the linearized scenario, smoothed Dirac delta functions appear at the
interface and in the adjoint solution the entire solution at the interface
becomes a plateau at the average value $0.5$. The extreme case of this can be
seen with the COMIC scheme. However, we have performed a simple optimization
test with a tracking-type cost functional and we have seen that both the
THINC and the COMIC scheme converge to the desired solution and capture even
a mollified interface to a satisfying degree.

The last chapter of this thesis investigated the definition of a more complex
optimization problem using a volume fraction description of the interface. The
problem we have chosen was to match the movement of our approximated interface
to a desired interface movement, defined using a level set function. The control
variables were the boundary conditions on a velocity potential defining an
incompressible and irrotational velocity field. This very simple example has
most of the characteristics we would expect in a the optimization of a two-phase
Navier-Stokes flow, without the difficulties arising from solving the complex
one-fluid model. We have seen how the derivation of the adjoint equations is
done in a multidimensional setting and the issues that we have to consider to
provide a well-posed problems.

The most difficult part in defining such an optimization problem is finding a
suitable cost functional. In our case, we have seen that formulating the
cost functional using a volume fraction defined by a Heaviside function is
not feasible at a continuous level. This lead us to consider a mollified
interface definition even at the continuous level. Numerically, this is also
a necessary choice, because interfaces identified by Dirac delta functions are
hard to deal with discretely. Using a smooth definition of the volume fraction,
allowed us to rigorously define our optimization problem by providing the
necessary vector spaces in which our solution and constraints must reside.
Unfortunately, the proposed cost functional is not convex, so proofs of
uniqueness of a optimal control is not possible. We have seen in numerical
examples that the control of the interface described in this way is indeed
possible. An interesting result was that, in the case when all the domain
boundaries are controlled, we could easily obtain different local minima of
our cost functional.

The results we have seen this far are very promising: we have managed to obtain
converging discrete adjoint solutions in the presence of different types of
discontinuities and use these results to control the movement of a fictitious
interface in a very simplified model. Future work is plentiful for problems
of this type. An important direction is finding an improved cost functional
that has unique solutions or at least removes trivial undesirable solutions.
Another obvious extension is replacing the velocity defined by a velocity potential
by the Navier-Stokes equations and the complete one-fluid model. Besides
applications related to optimizing the movement of the interface, this will
allow us to reproduce classic results regarding various instabilities and
move forward to more complicated and nonlinear problems.
