\chapter{Introduction}
\label{ch:introduction}

The present work is focused on developing the necessary prerequisites for the
optimization of incompressible two-phase flows. Specifically, the studies we will
perform have in mind future applications involving the commonly used one-fluid
model of two-phase flows. We will focus on using discrete adjoint methods,
instead of their continuous counterparts, partly due to their simplicity in
formulation (we only need to model the forward problem) and also due to the
consistency of the discrete gradients. However, this approach is not without
its share of trade-offs. To better understand these trade-offs, we will study
some of the issues that appear in these types of flows due to discontinuities
(e.g. contact discontinuities), the convergence to weak adjoint solutions, the
behavior of commonly used numerical schemes, with a focus on anti-diffusive
schemes, etc. Finally, we will attempt to formulate and solve an optimization
problem regarding the movement of an interface in an idealized scenario of an
incompressible and irrotational velocity field.

The type of the problem we will attempt to tackle is a PDE-constrained optimization
problem. This type of problem has gained a lot of interest in recent decades
with the advent of a rigorous formalism involving the optimal control of
dynamic systems, largely due to the work of Pontryagin and others~\cite{PBGM1962}.
Maybe the most prevalent of the methods used to solve this type of problem on a
large scale is the adjoint method. The advantage provided by the adjoint method
is that it offers an efficient way to compute the gradient of a given cost
functional, in a manner independent of the number of control variables. The
first applications of optimal control and the adjoint method to the field of
fluid mechanics were performed in the seminal papers by Olivier Pironneau~\cite{P1974},
who applied it to a simple shape optimization problem of a body in Stokes flow,
and Anthony Jameson~\cite{J1988}, who, several years later, extended the
approach to inviscid compressible flows. Ever since, optimal control and
adjoint methods have been one of the most important components of computational
fluid dynamics and related fields.

One of the most successful applications of adjoint methods has been in
topology optimization~\cite{NTBNH1999, AJT2004, BS2013}, where other methods
usually fail due to the significantly large size of the problem. Topology
optimization is also very prevalent in the field of aerospace engineering
with the works of~\cite{JMP1998, AV1999, GP2000} and others. However, adjoint
methods have been used in a very wide variety of applications, such as the
inverse problem in solid mechanics~\cite{OGF2003, BC2005},
seismology~\cite{FBI2006_1, FBI2006_2}, geophysics~\cite{P2006},
mesh adaptation~\cite{FD2011}, etc. More general approaches to optimal
control and adjoint-based optimization can be found in classic monographs such
as~\cite{L1971}, focusing on all major types of PDEs, notably hyperbolic
equations,~\cite{T2010, BS2012}, focusing on elliptic and parabolic equations,
or~\cite{G2003}, which offers a wide selection of examples using different types
of controls.

We are interested in extending approaches such as~\cite{GM1999}, which
study the optimal control of the velocity field described by the Navier-Stokes
equations, to the case of multiphase flows. To our knowledge, work on such
problems is in its very early stages in the context of incompressible
two-phase Navier-Stokes flows. As such, there are very few documented studies
that deal with the issues that can arise from optimizing multiphase flows and
methods to mitigate or solve these issues. Some other applications to general
multiphase media include the two-phase Stefan problem~\cite{BH2011}, various
studies in porous media~\cite{J2011}, rigid body motion in a multiphase
flow~\cite{SU2015}, etc. In the specific case of a two-phase one-fluid model,
we can immediately see a series of complications that can arise. Continuously,
we have to introduce new terms and equations into the classic Navier-Stokes
system related to the interface, modeling surface tension, mass transfer, etc.,
usually through a series of Heaviside functions and Dirac delta distributions.
Discretely, we have another set of issues related to an accurate representation
of a sharp fluid-fluid interface, accurate computation of surface tension, etc.
We will analyze some of these issues in very simplified scenarios, in an attempt
to gain intuition when approaching more realistic models. For an introduction
to the issues inherent in modeling multiphase flows see~\cite{PT2009, B2005}.

The first issue we will attempt to deal with is the rigorous definition of
the adjoint equations in the presence of discontinuities. Most descriptions of
adjoint methods make strong assumptions on the differentiability of all the
involved quantities. However, a sharp interface model is inherently discontinuous
and puts into question the well-posedness of the adjoint system and all the
manipulations performed to obtain it. Discontinuous solutions have been
thoroughly studied in the case of hyperbolic systems of conservation laws,
specifically in the context of shocks. Some early applications involving
such flows include~\cite{IS1999, HN2003, OONT1997}. Formally, the problem was
first approached by~\cite{BM1995}, which defined a new calculus for formulating
first-order optimality conditions using generalized tangent vectors. The
problem was later extended multiple times, for example in~\cite{BP2003, G2002,
U2001} and others. A recent important result can be found in~\cite{GU2010_1,
GU2010_2}, where a new modified Lax-Friedrichs scheme was defined, which
allowed rigorous proofs of convergence of the discrete linearized and adjoint
systems in the presence of multiple shocks. In our case, we are also interested
in the behavior of contact discontinuities, since the volume fraction gives rise to
one such wave pattern in the one-fluid model.

Besides discontinuities in the solutions to the two-phase problem, we also
encounter difficulties on a discrete level. Applying the
discretize-then-differentiate approach to adjoint methods (see~\cite{NJ2000,
G2002}) leads to differentiating highly nonlinear numerical schemes. In
multiphase flows, there are many complex numerical schemes that are in common
use, for example the VOF-family of methods~\cite{HN1981, RK1998}, the closely
related THINC-family of methods~\cite{XHK2005, Y2007}, the level set
methods~\cite{OS1988}, the front-tracking methods~\cite{UT1992}, etc. All these
methods are very complex and contain numerous non-differentiable terms, which
implies that their behavior under linearization is not clear. Furthermore,
rigorous results, such as the ones obtained using the modified Lax-Friedrichs
scheme, are out of reach even for the simplest of the previously mentioned
numerical schemes. Therefore, we will confine ourselves to numerical tests to
show the convergence of the adjoint scheme to known exact solutions.

Finally, an important issue in optimization in general is finding a well-posed
cost functional that faithfully represents the solutions we want to obtain
and has beneficial qualities, such as convexity. For our intended use in
the context of multiphase flows, we require formulating cost functionals that
involve the position of the interface or the volume fraction. These are
generally modeled using either Dirac delta functions or Heaviside functions,
both of which pose some problems numerically. We will see such a formulation,
the problems that come with it and how they can be solved. Furthermore, we
will show that controlling the movement of an interface is possible using
only the volume fraction. This is done on two levels: continuously, where a
rigorous approach to the existence and uniqueness of solutions to state and
adjoint equations is provided, and discretely, where we will show through a
series of tests that we can obtain a desired interface motion, even in
non-trivial cases.

\section*{Organization}

This thesis is organized into 6 chapters, out of which the current introduction
is the first. Chapter~\ref{ch:conclusions} contains the conclusions of our
analysis of discontinuous solutions to hyperbolic balance laws and applications
to interface motion planning.

In Chapter~\ref{ch:adjoint}, we will give a short introduction to adjoint methods,
with the goal of introducing common notation and current approaches to
optimization with adjoints. The most important part of adjoint methods is the
development of a calculus of variations in infinite dimensional vector spaces.
We will follow~\cite{BS2012, T2010} and introduce the ideas behind directional
derivatives and first-order optimality conditions in such spaces. This will
allow us to delve, in a rigorous manner, into the two different approaches to
adjoint methods: the~\emph{differentiate-then-discretize} approach and
the~\emph{discretize-then-differentiate} approach. Finally, we will shortly
investigate how the adjoint equations are obtained in these two scenarios,
under the assumption that all involved quantities are sufficiently smooth.

In Chapter~\ref{ch:discontinuities}, we will define the adjoint equations for
linear and nonlinear hyperbolic balance laws in the presence of discontinuities.
We will investigate the linear advection equation and Burgers' equation as two
representatives of the main issues that may arise from hyperbolic equations.
The main two problematic wave patterns exhibited by each of them are contact
discontinuities and shocks, respectively. The investigation will be performed
on two levels: continuous, where the definition of first-order variations of
discontinuous solutions is not trivial, and discrete, where convergence to
weak adjoint solutions and the non-differentiability of commonly used numerical
schemes are problematic.

In Chapter~\ref{ch:thinc}, we will move closer to two-phase flows by analyzing
the THINC family of schemes, a widely used set of numerical schemes with very
good anti-diffusive qualities. The THINC schemes are highly nonlinear
because of the use of a hyperbolic tangent in the definition of the numerical
flux. These nonlinearities cause many issues under linearization. We will
investigate the behavior of the THINC schemes and propose a series of
improvements that make them a viable choice when used in the context of the
discretize-then-differentiate approach to adjoint methods.

In Chapter~\ref{ch:motionplanning}, we will study the optimization of a simplified
two-phase flow problem. The problem consists in optimizing the movement of an
interface described by the advection of a volume fraction in an incompressible
and irrotational velocity field. Under these assumptions, the velocity field is
fully  described by a velocity potential. To reduce the dimensionality of the
problem even further, we will control the system using the boundary conditions
of the Laplace equation satisfied by the velocity potential. While this is not
a full two-phase model, it will allow us investigate issues related to the
formulation of such systems in the context of sharp interface models, where the
interface is represented by a Heaviside function. Numerical results are given
to prove the viability of the presented approach.
